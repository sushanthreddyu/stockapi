package com.neural_hack.stock_api.enums;

public enum APIKeys {
    COMPANY_PROFILE("btgv03748v6v983bbl7g"),
    STOCK_QUOTE("btqtjff48v6p4u78iiog"),
    STOCK_CANDLES("btqtmtn48v6p4u78inbg");

    private String apiKey;

    APIKeys(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiKey() {
        return apiKey;
    }
}
