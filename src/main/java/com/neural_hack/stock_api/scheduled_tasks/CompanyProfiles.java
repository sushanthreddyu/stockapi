package com.neural_hack.stock_api.scheduled_tasks;


import com.neural_hack.stock_api.enums.APIKeys;
import com.neural_hack.stock_api.models.CompanyProfileModel;
import com.neural_hack.stock_api.models.CompanySymbolModel;
import com.neural_hack.stock_api.repositories.CompanyProfilesRepository;
import com.neural_hack.stock_api.services.CompanySymbolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Service
public class CompanyProfiles {

    @Autowired
    private CompanySymbolService companySymbolService;

    @Autowired
    private CompanyProfilesRepository companyProfilesRepository;

    private List<CompanySymbolModel> companySymbolModelList = new ArrayList<>();

    private static int companyIndex = 0;

    @Scheduled(initialDelay = 1000, fixedDelay = 1000)
    public void getCompanyProfile() {
        if (companySymbolModelList.isEmpty()) {
            companySymbolModelList = companySymbolService.getCompanySymbols();
            return;
        }

        CompanySymbolModel companySymbolModel = companySymbolModelList.get(companyIndex);

        final String uri = "https://finnhub.io/api/v1/stock/profile2?symbol=" + companySymbolModel.getSymbol() + "&token=" + APIKeys.COMPANY_PROFILE.getApiKey();

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<CompanyProfileModel> result = restTemplate.getForEntity(uri, CompanyProfileModel.class);

        if (companyProfilesRepository.existsById(companySymbolModel.getSymbol())) {
            CompanyProfileModel companyProfileModel = companyProfilesRepository
                    .findById(companySymbolModel.getSymbol()).get();
            companyProfileModel.updateObject(Objects.requireNonNull(result.getBody()));
            companyProfilesRepository.save((companyProfileModel));
        } else {
            CompanyProfileModel companyProfileModel = result.getBody();
            assert companyProfileModel != null;
            companyProfileModel.setTicker(companySymbolModel.getSymbol());
            companyProfileModel.setCompanySymbol(companySymbolModel);

            companyProfilesRepository.save(companyProfileModel);
        }

        System.out.println("Company Profile update Successful");
        companyIndex = (companyIndex + 1) % 60;
    }
}
