package com.neural_hack.stock_api.scheduled_tasks;

import com.neural_hack.stock_api.enums.APIKeys;
import com.neural_hack.stock_api.models.CompanySymbolModel;
import com.neural_hack.stock_api.repositories.CompanyProfilesRepository;
import com.neural_hack.stock_api.repositories.CompanySymbolsRepository;
import com.neural_hack.stock_api.repositories.StockQuotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


// This Class is used to get all the company symbols
// Do not uncomment @Scheduled if you don't want to get all company symbols everyday

@Service
public class CompanySymbols {

    @Autowired
    private CompanySymbolsRepository companySymbolsRepository;

    @Autowired
    private CompanyProfilesRepository companyProfilesRepository;

    @Autowired
    private StockQuotesRepository stockQuotesRepository;

    @Scheduled(initialDelay = 1000, fixedRate = 86400000)
    public void getCompanySymbols() {
        if (companySymbolsRepository == null || !companySymbolsRepository.findAll().iterator().hasNext()) {
            final String uri = "https://finnhub.io/api/v1/stock/symbol?exchange=US&token=" + APIKeys.COMPANY_PROFILE.getApiKey();

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<CompanySymbolModel[]> result = restTemplate.getForEntity(uri, CompanySymbolModel[].class);

            CompanySymbolModel[] companySymbolModels = result.getBody();
            int cnt = 0;
            assert companySymbolModels != null;
            for (CompanySymbolModel symbol : companySymbolModels)
                if (!symbol.getDescription().equals("") && !symbol.getCurrency().equals("") && !symbol.getType().equals("")) {
                    companySymbolsRepository.save(symbol);
                    cnt++;
                    if (cnt == 60) break;
                }
            System.out.println("Company Symbols Inserted");
        }
    }
}
