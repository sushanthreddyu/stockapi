package com.neural_hack.stock_api.scheduled_tasks;

import com.neural_hack.stock_api.enums.APIKeys;
import com.neural_hack.stock_api.models.CompanySymbolModel;
import com.neural_hack.stock_api.models.StockQuoteModel;
import com.neural_hack.stock_api.repositories.StockQuotesRepository;
import com.neural_hack.stock_api.services.CompanySymbolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class StockQuotes {

    @Autowired
    private StockQuotesRepository stockQuotesRepository;

    @Autowired
    private CompanySymbolService companySymbolService;

    private List<CompanySymbolModel> companySymbolsListModel = new ArrayList<>();
    private static int companyIndex = 0;

    @Scheduled(initialDelay = 1000, fixedDelay = 1000)
    public void getStockQuotes() {
        if (companySymbolsListModel.isEmpty()) {
            companySymbolsListModel = companySymbolService.getCompanySymbols();
            return;
        }

        CompanySymbolModel companySymbolModel = companySymbolsListModel.get(companyIndex);

        String uri = "https://finnhub.io/api/v1/quote?symbol=" + companySymbolModel.getSymbol() + "&token=" + APIKeys.STOCK_QUOTE.getApiKey();

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<StockQuoteModel> result = restTemplate.getForEntity(uri, StockQuoteModel.class);

        if (stockQuotesRepository.existsById(companySymbolModel.getSymbol())) {
            StockQuoteModel stockQuoteModel = stockQuotesRepository.findById(companySymbolModel.getSymbol()).get();
            stockQuoteModel.updateValues(Objects.requireNonNull(result.getBody()));
            stockQuotesRepository.save(stockQuoteModel);
        } else {
            StockQuoteModel stockQuoteModel = result.getBody();
            assert stockQuoteModel != null;
            stockQuoteModel.setSymbol(companySymbolModel.getSymbol());
            stockQuoteModel.setCompanySymbol(companySymbolModel);
            stockQuotesRepository.save(stockQuoteModel);
        }

        companyIndex = (companyIndex + 1) % 60;

    }
}
