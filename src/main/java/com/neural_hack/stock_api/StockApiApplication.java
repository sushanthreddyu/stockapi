package com.neural_hack.stock_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class StockApiApplication {

    public static void main(String[] args) {

        SpringApplication.run(StockApiApplication.class, args);
        System.out.println("Main Function");
    }

}
