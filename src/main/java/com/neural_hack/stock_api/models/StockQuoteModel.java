package com.neural_hack.stock_api.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "stock_quotes")
public class StockQuoteModel implements Serializable {
    @Id
    private String symbol;

    @Column(name = "open_price")
    private Double o;

    @Column(name = "high_price")
    private Double h;

    @Column(name = "low_price")
    private Double l;

    @Column(name = "current_price")
    private Double c;

    @Column(name = "previous_close_price")
    private Double pc;

    @OneToOne(cascade = CascadeType.ALL)
    @MapsId
    private CompanySymbolModel companySymbolModel;

    public StockQuoteModel() {
    }

    public StockQuoteModel(String symbol, Double o, Double h, Double l, Double c, Double pc) {
        this.symbol = symbol;
        this.o = o;
        this.h = h;
        this.l = l;
        this.c = c;
        this.pc = pc;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Double getO() {
        return o;
    }

    public void setO(Double o) {
        this.o = o;
    }

    public Double getH() {
        return h;
    }

    public void setH(Double h) {
        this.h = h;
    }

    public Double getL() {
        return l;
    }

    public void setL(Double l) {
        this.l = l;
    }

    public Double getC() {
        return c;
    }

    public void setC(Double c) {
        this.c = c;
    }

    public Double getPc() {
        return pc;
    }

    public void setPc(Double pc) {
        this.pc = pc;
    }

    public CompanySymbolModel getCompanySymbol() {
        return companySymbolModel;
    }

    public void setCompanySymbol(CompanySymbolModel companySymbolModel) {
        this.companySymbolModel = companySymbolModel;
    }

    public void updateValues(StockQuoteModel stockQuoteModel) {
        this.o = stockQuoteModel.getO();
        this.h = stockQuoteModel.getH();
        this.l = stockQuoteModel.getL();
        this.c = stockQuoteModel.getC();
        this.pc = stockQuoteModel.getPc();
    }

    @Override
    public String toString() {
        return "StockQuote{" +
                "symbol='" + symbol + '\'' +
                ", o=" + o +
                ", h=" + h +
                ", l=" + l +
                ", c=" + c +
                ", pc=" + pc +
                ", companySymbol=" + companySymbolModel +
                '}';
    }
}
