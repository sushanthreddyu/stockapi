package com.neural_hack.stock_api.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "company_profiles")
public class CompanyProfileModel {
    @Id
    private String ticker;

    private String country;
    private String currency;
    private String exchange;
    @Column(name = "industry")
    private String finnhubIndustry;
    private Date ipo;
    private String logo;
    private Long marketCapitalization;
    private String name;
    private String phone;
    private Double shareOutstanding;
    private String weburl;

    @OneToOne
    @MapsId
    private CompanySymbolModel companySymbolModel;

    public CompanyProfileModel() {
    }

    public CompanyProfileModel(String country, String currency, String exchange, String finnhubIndustry, Date ipo, String logo, Long marketCapitalization, String name, String phone, Double shareOutstanding, String ticker, String weburl) {
        this.country = country;
        this.currency = currency;
        this.exchange = exchange;
        this.finnhubIndustry = finnhubIndustry;
        this.ipo = ipo;
        this.logo = logo;
        this.marketCapitalization = marketCapitalization;
        this.name = name;
        this.phone = phone;
        this.shareOutstanding = shareOutstanding;
        this.ticker = ticker;
        this.weburl = weburl;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getFinnhubIndustry() {
        return finnhubIndustry;
    }

    public void setFinnhubIndustry(String finnhubIndustry) {
        this.finnhubIndustry = finnhubIndustry;
    }

    public Date getIpo() {
        return ipo;
    }

    public void setIpo(Date ipo) {
        this.ipo = ipo;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Long getMarketCapitalization() {
        return marketCapitalization;
    }

    public void setMarketCapitalization(Long marketCapitalization) {
        this.marketCapitalization = marketCapitalization;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Double getShareOutstanding() {
        return shareOutstanding;
    }

    public void setShareOutstanding(Double shareOutstanding) {
        this.shareOutstanding = shareOutstanding;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getWeburl() {
        return weburl;
    }

    public void setWeburl(String weburl) {
        this.weburl = weburl;
    }

    public CompanySymbolModel getCompanySymbol() {
        return companySymbolModel;
    }

    public void setCompanySymbol(CompanySymbolModel companySymbolModel) {
        this.companySymbolModel = companySymbolModel;
    }

    public void updateObject(CompanyProfileModel newCompanyProfileModel) {
        this.setCountry(newCompanyProfileModel.getCountry());
        this.setCurrency(newCompanyProfileModel.getCurrency());
        this.setExchange(newCompanyProfileModel.getExchange());
        this.setFinnhubIndustry(newCompanyProfileModel.getFinnhubIndustry());
        this.setIpo(newCompanyProfileModel.getIpo());
        this.setLogo(newCompanyProfileModel.getLogo());
        this.setMarketCapitalization(newCompanyProfileModel.getMarketCapitalization());
        this.setName(newCompanyProfileModel.getName());
        this.setPhone(newCompanyProfileModel.getPhone());
        this.setShareOutstanding(newCompanyProfileModel.getShareOutstanding());
        this.setWeburl(newCompanyProfileModel.getWeburl());
    }

    @Override
    public String toString() {
        return "CompanyProfile{" +
                "ticker='" + ticker + '\'' +
                ", country='" + country + '\'' +
                ", currency='" + currency + '\'' +
                ", exchange='" + exchange + '\'' +
                ", finnhubIndustry='" + finnhubIndustry + '\'' +
                ", ipo=" + ipo +
                ", logo='" + logo + '\'' +
                ", marketCapitalization=" + marketCapitalization +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", shareOutstanding=" + shareOutstanding +
                ", weburl='" + weburl + '\'' +
                ", companySymbol=" + companySymbolModel +
                '}';
    }
}
