package com.neural_hack.stock_api.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "company_symbols")
public class CompanySymbolModel implements Serializable {
    @Id
    private String symbol;
    private String description;
    private String displaySymbol;
    private String type;
    private String currency;

    public CompanySymbolModel() {
    }

    public CompanySymbolModel(String description, String displaySymbol, String symbol, String type, String currency) {
        this.description = description;
        this.displaySymbol = displaySymbol;
        this.symbol = symbol;
        this.type = type;
        this.currency = currency;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplaySymbol() {
        return displaySymbol;
    }

    public void setDisplaySymbol(String displaySymbol) {
        this.displaySymbol = displaySymbol;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

//    public CompanyProfile getCompanyProfile() {
//        return companyProfile;
//    }
//
//    public void setCompanyProfile(CompanyProfile companyProfile) {
//        this.companyProfile = companyProfile;
//    }
//
//    public StockQuote getStockQuote() {
//        return stockQuote;
//    }
//
//    public void setStockQuote(StockQuote stockQuote) {
//        this.stockQuote = stockQuote;
//    }

    @Override
    public String toString() {
        return "CompanySymbol{" +
                "symbol='" + symbol + '\'' +
                ", description='" + description + '\'' +
                ", displaySymbol='" + displaySymbol + '\'' +
                ", type='" + type + '\'' +
                ", currency='" + currency + '\'' +
                '}';
    }
}

