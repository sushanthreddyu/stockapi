package com.neural_hack.stock_api.models;

import javax.persistence.Column;

public class StockQuote {
    private String symbol;
    private Double o;
    private Double h;
    private Double l;
    private Double c;
    private Double pc;

    public StockQuote() {
    }

    public StockQuote(String symbol, Double o, Double h, Double l, Double c, Double pc) {
        this.symbol = symbol;
        this.o = o;
        this.h = h;
        this.l = l;
        this.c = c;
        this.pc = pc;
    }

    public StockQuote(StockQuoteModel stockQuoteModel){
        this.symbol = stockQuoteModel.getSymbol();
        this.o = stockQuoteModel.getO();
        this.h = stockQuoteModel.getH();
        this.l = stockQuoteModel.getL();
        this.c = stockQuoteModel.getC();
        this.pc = stockQuoteModel.getPc();
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Double getO() {
        return o;
    }

    public void setO(Double o) {
        this.o = o;
    }

    public Double getH() {
        return h;
    }

    public void setH(Double h) {
        this.h = h;
    }

    public Double getL() {
        return l;
    }

    public void setL(Double l) {
        this.l = l;
    }

    public Double getC() {
        return c;
    }

    public void setC(Double c) {
        this.c = c;
    }

    public Double getPc() {
        return pc;
    }

    public void setPc(Double pc) {
        this.pc = pc;
    }
}
