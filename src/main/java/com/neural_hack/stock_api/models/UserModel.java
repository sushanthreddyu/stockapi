package com.neural_hack.stock_api.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "user")
public class UserModel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String first_name;
    private String last_name;
    @Column(unique = true)
    private String email;
    @Column(unique = true)
    private String phno;
    @Column(unique = true)
    private String username;
    private String password;
    @Column(unique = true)
    private String apiKey;
    private Boolean premium_user;

    public UserModel() {
    }

    public UserModel(String first_name, String last_name, String email, String phno, String username, String password) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.phno = phno;
        this.username = username;
        this.password = password;
    }


    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhno() {
        return phno;
    }

    public void setPhno(String phno) {
        this.phno = phno;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }


    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getPremium_user() {
        return premium_user;
    }

    public void setPremium_user(Boolean premium_user) {
        this.premium_user = premium_user;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String api_key) {
        this.apiKey = api_key;
    }

    public Long getId() {
        return id;
    }

}
