package com.neural_hack.stock_api.models;

import java.util.Date;

public class CompanyProfile {
    private String ticker;
    private String country;
    private String currency;
    private String exchange;
    private String finnhubIndustry;
    private Date ipo;
    private String logo;
    private Long marketCapitalization;
    private String name;
    private String phone;
    private Double shareOutstanding;
    private String weburl;

    public CompanyProfile() {
    }

    public CompanyProfile(String ticker, String country, String currency, String exchange, String finnhubIndustry, Date ipo, String logo, Long marketCapitalization, String name, String phone, Double shareOutstanding, String weburl) {
        this.ticker = ticker;
        this.country = country;
        this.currency = currency;
        this.exchange = exchange;
        this.finnhubIndustry = finnhubIndustry;
        this.ipo = ipo;
        this.logo = logo;
        this.marketCapitalization = marketCapitalization;
        this.name = name;
        this.phone = phone;
        this.shareOutstanding = shareOutstanding;
        this.weburl = weburl;
    }

    public CompanyProfile(CompanyProfileModel companyProfileModel){
        this.ticker = companyProfileModel.getTicker();
        this.country = companyProfileModel.getCountry();
        this.currency = companyProfileModel.getCurrency();
        this.exchange = companyProfileModel.getExchange();
        this.finnhubIndustry = companyProfileModel.getFinnhubIndustry();
        this.ipo = companyProfileModel.getIpo();
        this.logo = companyProfileModel.getLogo();
        this.marketCapitalization = companyProfileModel.getMarketCapitalization();
        this.name = companyProfileModel.getName();
        this.phone = companyProfileModel.getPhone();
        this.shareOutstanding = companyProfileModel.getShareOutstanding();
        this.weburl = companyProfileModel.getWeburl();
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getFinnhubIndustry() {
        return finnhubIndustry;
    }

    public void setFinnhubIndustry(String finnhubIndustry) {
        this.finnhubIndustry = finnhubIndustry;
    }

    public Date getIpo() {
        return ipo;
    }

    public void setIpo(Date ipo) {
        this.ipo = ipo;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Long getMarketCapitalization() {
        return marketCapitalization;
    }

    public void setMarketCapitalization(Long marketCapitalization) {
        this.marketCapitalization = marketCapitalization;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Double getShareOutstanding() {
        return shareOutstanding;
    }

    public void setShareOutstanding(Double shareOutstanding) {
        this.shareOutstanding = shareOutstanding;
    }

    public String getWeburl() {
        return weburl;
    }

    public void setWeburl(String weburl) {
        this.weburl = weburl;
    }
}