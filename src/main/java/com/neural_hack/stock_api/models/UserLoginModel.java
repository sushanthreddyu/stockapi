package com.neural_hack.stock_api.models;

public class UserLoginModel {
    private String username;
    private String password;

    public UserLoginModel() {
    }

    public UserLoginModel(UserLoginModel o) {
        this.setPassword(o.password);
        this.setUsername(o.username);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
