package com.neural_hack.stock_api.services;

import com.neural_hack.stock_api.models.CompanySymbolModel;
import com.neural_hack.stock_api.repositories.CompanySymbolsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("companySymbolService")
public class CompanySymbolService {
    @Autowired
    private CompanySymbolsRepository companySymbolsRepository;

    public List<CompanySymbolModel> getCompanySymbols() {
        List<CompanySymbolModel> companySymbolsListModel = new ArrayList<>();
        Iterable<CompanySymbolModel> allCompanySymbols = companySymbolsRepository.findAll();
        allCompanySymbols.forEach(companySymbolsListModel::add);
        /*
         *   Other Feasible option is to use Streams to ingest the values into the List
         *   companySymbolList = StreamSupport.stream(allCompanySymbols.spliterator(), false).collect(Collectors.toList());
         * */
        return companySymbolsListModel;
    }
}
