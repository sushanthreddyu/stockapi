package com.neural_hack.stock_api.services;

import com.neural_hack.stock_api.models.UserModel;
import com.neural_hack.stock_api.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Collections;
import java.util.Objects;
import java.util.UUID;

@Service("userService")
public class UserService implements UserDetailsService {

//    private static final char[] hexArray = "0123456789ABCDEF".toCharArray();

    @Autowired
    private UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public void saveUser(final UserModel userModelData) {
        userRepository.save(Objects.requireNonNull(populateUserData(userModelData)));
    }

//    public Boolean authenticateUser(final String username, final String password) {
//        UserModel userModel = userRepository.findUserModelByUsername(username);
//        return passwordEncoder.matches(password, userModel.getPassword());
//    }

    public UserModel getUser(final String username) {
        return userRepository.findUserModelByUsername(username);
    }

    private UserModel populateUserData(final UserModel userModelData) {
        UserModel userModel = new UserModel();
        userModel.setUsername(userModelData.getUsername());
        userModel.setEmail(userModelData.getEmail());
        userModel.setFirst_name(userModelData.getFirst_name());
        userModel.setLast_name((userModelData.getLast_name()));
        userModel.setPhno(userModelData.getPhno());
        userModel.setPassword((passwordEncoder.encode(userModelData.getPassword())));
        userModel.setPremium_user(userModelData.getPremium_user());

        return generateNewAPIKey(userModel);
    }

    public static UserModel generateNewAPIKey(UserModel userModel) {
        try {
            userModel.setApiKey(generateUniqueKeysWithUUIDAndMessageDigest(userModel.getPremium_user()));
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
        return userModel;
    }

    public Boolean verifyAPIKey(String apiKey) {
        UserModel user = userRepository.findUserModelByApiKey(apiKey);
        return user != null;
    }

    /**
     * Unique Keys Generation Using Message Digest and Type 4 UUID
     *
     * @throws NoSuchAlgorithmException
     */
    private static String generateUniqueKeysWithUUIDAndMessageDigest(Boolean isPremium) throws NoSuchAlgorithmException {
        UUID uuid = UUID.randomUUID();
        MessageDigest salt = MessageDigest.getInstance("SHA-256");
        salt.update(uuid.toString().getBytes(StandardCharsets.UTF_8));
        byte[] base64 = Base64.getEncoder().encode(salt.digest());
        String api_key = new String(base64);
        if (isPremium) {
            return "PX-" + api_key;
        } else {
            return "FX-" + api_key;
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserModel user = userRepository.findUserModelByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new User(user.getUsername(), user.getPassword(), Collections.emptyList());
    }

//    Retaining for Future uses
//    private static String bytesToHex(byte[] bytes) {
//        char[] hexChars = new char[bytes.length * 2];
//        for (int j = 0; j < bytes.length; j++) {
//            int v = bytes[j] & 0xFF;
//            hexChars[j * 2] = hexArray[v >>> 4];
//            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
//        }
//        return new String(hexChars);
//    }
}
