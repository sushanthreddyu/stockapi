package com.neural_hack.stock_api.services;

import com.neural_hack.stock_api.models.StockQuote;
import com.neural_hack.stock_api.models.StockQuoteModel;
import com.neural_hack.stock_api.repositories.StockQuotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StockQuoteService {
    @Autowired
    private StockQuotesRepository stockQuotesRepository;

    public StockQuote getStockQuote(String symbol) {
        return new StockQuote(stockQuotesRepository.findById(symbol).get());
    }
}
