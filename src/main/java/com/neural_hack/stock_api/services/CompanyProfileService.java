package com.neural_hack.stock_api.services;

import com.neural_hack.stock_api.models.CompanyProfile;
import com.neural_hack.stock_api.models.CompanyProfileModel;
import com.neural_hack.stock_api.repositories.CompanyProfilesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("companyProfileService")
public class CompanyProfileService {
    @Autowired
    private CompanyProfilesRepository companyProfilesRepository;

    public CompanyProfile getCompanyProfile(String symbol) {
        return new CompanyProfile(companyProfilesRepository.findById(symbol).get());
    }
}
