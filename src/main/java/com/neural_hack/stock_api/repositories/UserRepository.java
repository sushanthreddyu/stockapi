package com.neural_hack.stock_api.repositories;

import com.neural_hack.stock_api.models.UserModel;
import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface UserRepository extends CrudRepository<UserModel, Long> {
    UserModel findUserModelByUsername(String username);
    UserModel findUserModelByApiKey(String apiKey);
}
