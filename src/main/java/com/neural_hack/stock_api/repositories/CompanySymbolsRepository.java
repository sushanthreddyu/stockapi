package com.neural_hack.stock_api.repositories;

import org.springframework.data.repository.CrudRepository;

import com.neural_hack.stock_api.models.CompanySymbolModel;

// This will be AUTO IMPLEMENTED by Spring into a Bean called companySymbolsRepository
// CRUD refers Create, Read, Update, Delete

public interface CompanySymbolsRepository extends CrudRepository<CompanySymbolModel, String> {
}






