package com.neural_hack.stock_api.repositories;

import com.neural_hack.stock_api.models.CompanyProfileModel;
import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called companyProfilesRepository
// CRUD refers Create, Read, Update, Delete

public interface CompanyProfilesRepository extends CrudRepository<CompanyProfileModel, String> {
}






