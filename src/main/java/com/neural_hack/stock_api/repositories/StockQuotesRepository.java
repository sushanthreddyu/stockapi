package com.neural_hack.stock_api.repositories;

import com.neural_hack.stock_api.models.StockQuoteModel;
import org.springframework.data.repository.CrudRepository;

public interface StockQuotesRepository extends CrudRepository<StockQuoteModel, String> {
}
