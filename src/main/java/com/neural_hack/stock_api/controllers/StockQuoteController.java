package com.neural_hack.stock_api.controllers;

import com.neural_hack.stock_api.models.StockQuote;
import com.neural_hack.stock_api.services.StockQuoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.NoSuchElementException;

@RestController
public class StockQuoteController {

    @Autowired
    private StockQuoteService stockQuoteService;

    @GetMapping("/quote")
    public ResponseEntity<?> getStockQuote(@RequestParam(name = "symbol") String symbol) {
        try {
            return ResponseEntity.accepted().body(stockQuoteService.getStockQuote(symbol));
        } catch (NoSuchElementException e) {
            return ResponseEntity.badRequest().body(HttpStatus.NOT_FOUND);
        }
    }
}