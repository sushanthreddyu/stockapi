package com.neural_hack.stock_api.controllers;

import com.neural_hack.stock_api.models.CompanyProfile;
import com.neural_hack.stock_api.models.CompanyProfileModel;
import com.neural_hack.stock_api.services.CompanyProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.NoSuchElementException;

@RestController
public class CompanyProfileController {

    @Autowired
    private CompanyProfileService companyProfileService;

    @GetMapping("/company_profile")
    public ResponseEntity<?> getCompanyProfile(@RequestParam(name = "symbol") String companySymbol) {
        try {
            return ResponseEntity.accepted().body(companyProfileService.getCompanyProfile(companySymbol));
        } catch (NoSuchElementException e) {
            return ResponseEntity.badRequest().body(HttpStatus.NOT_FOUND);
        }
    }
}


