package com.neural_hack.stock_api.controllers;

import com.neural_hack.stock_api.models.UserModel;
import com.neural_hack.stock_api.models.UserLoginModel;
import com.neural_hack.stock_api.services.UserService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/user")
    public ResponseEntity<UserModel> getUser(@RequestParam String username) {
        UserModel user = null;
        try {
            user = userService.getUser(username);
            user.setPassword(null);
        } catch (Exception e) {
            user = null;
            System.out.println("Error while getting User: " + e);
        }
        if(user == null){
            return ResponseEntity.badRequest().body(null);
        }else {
            return ResponseEntity.ok().body(user);
        }
    }

    @PostMapping("/register")
    public ResponseEntity<HttpStatus> register(@RequestBody UserModel userModel) {
        try {
            userService.saveUser(userModel);
            return ResponseEntity.ok(HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }
}
