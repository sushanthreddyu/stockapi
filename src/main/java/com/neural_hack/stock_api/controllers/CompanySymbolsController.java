package com.neural_hack.stock_api.controllers;

import com.neural_hack.stock_api.models.CompanySymbolModel;
import com.neural_hack.stock_api.services.CompanySymbolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
public class CompanySymbolsController {

    @Autowired
    private CompanySymbolService companySymbolService;

    @GetMapping("/company_symbols")
    public ResponseEntity<?> getCompanySymbols() {
        try {
            return ResponseEntity.accepted().body(companySymbolService.getCompanySymbols());
        } catch (NoSuchElementException e) {
            return ResponseEntity.badRequest().body(HttpStatus.NOT_FOUND);
        }
    }
}